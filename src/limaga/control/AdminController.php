<?php


namespace limaga\control;

use \limaga\models\Client as Client;
use limaga\views\VueClient;
use \Slim\Slim as Slim;

class AdminController extends AbstractController {



    public function register()
    {
        $app = Slim::getInstance();
        $cli = new Client();

        // remplissage des attributs du nouveau client
        $cli->nom = filter_var($app->request->post('nom'),FILTER_SANITIZE_STRING);
        $cli->prenom = filter_var($app->request->post('prenom'),FILTER_SANITIZE_STRING);
        $cli->datenaiss = filter_var($app->request->post('datenaiss'),FILTER_SANITIZE_STRING);
        $cli->adresse = filter_var($app->request->post('adresse'),FILTER_SANITIZE_STRING);
        $cli->ville = filter_var($app->request->post('ville'), FILTER_SANITIZE_STRING);
        $cli->code_postal = filter_var($app->request->post('code_postal'), FILTER_SANITIZE_STRING);
        $cli->mail = filter_var($app->request->post('mail'),FILTER_SANITIZE_EMAIL);
        $cli->mdp = password_hash($app->request->post('password'), PASSWORD_DEFAULT, array('cost' => 12));
        $cli->telephone = filter_var($app->request->post('telephone'), FILTER_SANITIZE_STRING);
        $cli->lvl_natation = filter_var($app->request->post('niv_natation'),FILTER_SANITIZE_NUMBER_INT);

        $cli->save();

        $app->redirect($app->urlFor('connexionForm'));

    }

    public function inscriptionForm()
    {
        $v = new VueClient(null);
        $v->render(4);
    }

    /**
     * permet à un utilisateur de se connecter
     */
    public function logIn()
    {
        $app = Slim::getInstance();
        //recuperation des donnees de connexion filtrees
        $mail = $app->request->post('mail');
        $mdp = $app->request->post('mdp');

        $cli = Client::where('mail', 'like', $mail)->first();
        if (password_verify($mdp, $cli->mdp)) {
                $_SESSION['client'] = $cli;
        }

        // attribution d'un panier au client
        if(!isset($_SESSION['panier'])) {
            $_SESSION['panier'] = array();
        }

        // redirection
        if (isset($_SESSION['client']))
        {
            $app->redirect("http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['SCRIPT_NAME']));
        }
        else
        {
            $app->redirect("http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['SCRIPT_NAME']) . "/connexion/error");
        }
    }

    public function logOut()
    {
        $app = Slim::getInstance();

        $_SESSION = array();
        session_destroy();
        $app->redirect("http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['SCRIPT_NAME']));
    }

    // gere le formulaire lors de la connexion de l'utilisateur
    public function loginForm()
    {
        $v = new VueClient();
        $v->render(5);
    }

    public function loginError()
    {
        $v = new VueClient();
        $v->render(6);
    }

    public function addFamille()
    {
        $app = Slim::getInstance();

    }
}