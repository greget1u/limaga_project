<?php


namespace limaga\control;

use limaga\views\VueClient as VueClient;
use limaga\models\Abonnement as Abonnement;
use \limaga\models\Billet as Billet;



class ClientController extends AbstractController{


    public function home()
    {
        $v = new VueClient();
        $v->render(0);
    }

    public function listAbonnement($id_cli)
    {
        $lista = Abonnement::where('id_client', 'like', $id_cli)->get();
        $v = new VueClient($lista);
        $v->render(2);
    }

    public function listBillet($id_cli)
    {
        $listb = Billet::where('id_client', 'like', $id_cli)->get();
        $v = new VueClient($listb);
        $v->render(3);

    }

    public function achatEntrees()
    {
        $v = new VueClient();
        $v->render(8);
    }

    public function afficherPanier()
    {
        $v = new VueClient($_SESSION['panier']);
        $v->render(9);
    }


    public function identificationError()
    {
        $c = new VueClient();
        $c->render(10);
    }



}