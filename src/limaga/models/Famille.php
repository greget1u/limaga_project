<?php
/**represente un membre de la famille */

namespace limaga\models;


class Famille extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'famille';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function client()
    {
        return $this->belongsTo('limaga\models\Client', 'id_client');
    }

}