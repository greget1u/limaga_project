<?php
/**
 * Created by PhpStorm.
 * User: Antoine
 * Date: 11/06/2015
 * Time: 18:53
 */

namespace limaga\models;


class Abonnement extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'abonnement';
    protected $primaryKey = 'id';
    public $timestamps = false;

}