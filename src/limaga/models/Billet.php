<?php
/**
 * Created by PhpStorm.
 * User: Antoine
 * Date: 11/06/2015
 * Time: 18:54
 */

namespace limaga\models;


class Billet extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'billet';
    protected $primaryKey = 'id';
    public $timestamps = false;

}