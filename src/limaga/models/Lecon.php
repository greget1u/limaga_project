<?php


namespace limaga\models;


class Lecon extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'lecon';
    protected $primaryKey = 'id';
    public $timestamps = false;

}