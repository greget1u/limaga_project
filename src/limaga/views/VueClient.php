<?php

namespace limaga\views;


use limaga\models\Materiel;

class VueClient {


    protected $tab;

    public function __construct($t=null)
    {
        if(!is_null($t))
        {
            $this->tab = $t;
        }
    }

    public function render($sel)
    {
        $content = "";
        $he = $this->header();
        switch($sel)
        {
            case 0 :
                $content = $this->home();
                break;
            // remplissage formulaire membres famille
            case 1:
                $content=$this->formFamille();
                break;
            // affichage du panier
            case 2:
                $content=$this->abonnements();
                break;
            case 3 :
                $content = $this->billets();
                break;
            case 4:
                $content = $this->formInscription();
                break;
            case 5 :
                $content = $this->formConnexion();
                break;
            case 6:
                $content = $this->errorConnexion();
                break;
            case 7 :
                $content = $this->listMateriel();
                break;
            case 8 :
                $content = $this->listEntrees();
                break;
            case 9 :
                $content = $this->panier();
                break;
            case 10 :
                $content = $this->errorIdentification();
                break;
            default :
                $content = $this->home();
        }
        $html = <<<END
<html>
    <head>
        <meta charset="utf-8" />
        <title>Limaga</title>
		<link rel=StyleSheet type="text/css" href="web/css/Accueil.css">
		<script type="text/javascript" src="test.js"></script>
    </head>
    $he
    <body>
    <nav id="menu">
    <div class="element_menu">
        <ul id="menu">
        <li>
                <a href="accueil">Accueil</a>
        </li>
        <li>
                <a href="#">Membres</a>
                <ul>
                        <li><a href="connection">Connexion</a></li>
                        <li><a href="inscription">Inscription</a></li>
                </ul>
        </li>
        <li>
                <a href="#">Actualités</a>
                <ul>
                        <li><a href="photos">Photos</a></li>
                        <li><a href="videos">Vidéos</a></li>
                </ul>
        </li>
        <li>
                <a href="#">Divers</a>
                <ul>
                        <li><a href="informations">Informations</a></li>
                        <li><a href="contacts">Contacts</a></li>
                        <li><a href="plan">Plan d'accès</a></li>
						<li><a href="tarifs">Tarifs</a></li>
                </ul>
        </li>
</ul>
    </div>
</nav>
    <div id="corps">
        $content

    </div>
  </body>
  <footer id="pied_de_page">
        <p>Copyright Limaga Corp.</p>
  </footer>
</html>



END;
        echo $html;


    }

    private function header()
    {
        $h = "<header>";
        if (isset($_SESSION['client']))
        {
            $h.="<p>Bonjour ". $_SESSION['client']->prenom . " " . $_SESSION['client']->nom."</p>";
        }
        $h.="</header>";
        return $h;
    }

    private function home()
    {
        $app = \Slim\Slim::getInstance();
        $insc = $app->urlFor('inscriptionForm');
        $conn = $app->urlFor('connexionForm');

        if (!isset($_SESSION['client']))
        {
            $html = <<<END
         <section>
			<p class="titre"> Bienvenue sur le portail ! </p>
			<article class="index">
				<p class="inscription">
				<a href=$insc><p class="p2"> Inscription </p></a>
				</p>
			</article>

			<article class="index">
			<p class="connection">
			<a href=$conn> Se Connecter </p></a>
			</p>
			</article>
		</section>
END;
        }
        else
        {
            $html = <<<END
        <section>
			<p class="titre"> Bienvenue sur le portail ! </p>
			<article class="index">
				<p> Maintenant que vous êtes connecté(e), vous pouvez accéder à tous nos services en ligne, incluant
				    l'achats d'entrées pour la piscine, de produits dérivés, la réservation de cours particuliers ou bien
				    en groupe, et bien plus encore !
                </p>
			</article>
		</section>
END;
        }


        return $html;
    }


    private function formInscription()
    {
        $app = \Slim\Slim::getInstance();
        $url = $app->urlFor("inscription");

        $html = <<<END
<div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1>Inscription au portail
              <br>
            </h1>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <h1>Afin de vous inscrire, veuillez remplir le formulaire suivant</h1>
            <p></p>
          </div>
          <div class="col-md-6">
            <div class="col-md-12">
              <form action="$url" method="post" role="form">
                <div class="form-group">
                  <label class="control-label" for="prenom">Prenom</label>
                  <input class="form-control" id="prenom" name="prenom" placeholder="Prenom"
                  type="text">
                </div>
                <div class="form-group">
                  <label class="control-label" for="nom">Nom</label>
                  <input id="nom" name="nom" placeholder="Nom" type="text" class="form-control">
                </div>
                <div class="form-group">
                  <label class="control-label" for="datenaiss">Date de naissance</label>
                  <input class="form-control" id="datenaiss" name="datenaiss" placeholder="YYYY-MM-JJ" type="date">
                </div>
                <div class="form-group">
                  <label class="control-label" for="mail">Email</label>
                  <input class="form-control" id="mail" name="mail"placeholder="E-Mail"
                  type="email">
                </div>
                <div class="form-group">
                  <label class="control-label" for="password">Mot de passe</label>
                  <input class="form-control" id="password" name="password"placeholder="Mot de passe"
                  type="password">
                </div>
                <div class="form-group">
                  <label class="control-label" for="adresse">Adresse</label>
                  <input id="adresse" name="adresse"placeholder="Adresse" type="text"
                  class="form-control">
                </div>
                <div class="form-group">
                  <label class="control-label" for="ville">Ville</label>
                  <input class="form-control" id="ville" name="ville" placeholder="Ville"
                  type="text">
                </div>
                <div class="form-group">
                  <label class="control-label" for="code_postal">Code postal</label>
                  <input class="form-control" id="code_postal" name="code_postal" placeholder="Code postal"
                  type="text">
                </div>
                <div class="form-group">
                  <label class="control-label" for="telephone">Telephone</label>
                  <input class="form-control" id="telephone" name="telephone" placeholder="N° telephone"
                  type="text">
                </div>
                <div class="form-group">
                  <label class="control-label">Niveau de natation</label>
                  <select name="niv_natation" class="form-control">
                    <option value="1">Débutant</option>
                    <option value="2">Intermédiaire</option>
                    <option value="3">Avancé</option>
                    <option value="4">Professionnel</option>
                  </select>
                </div>
                <button type="submit" class="btn btn-info">Valider</button>
              </form>
            </div>
            <p></p>
            <p></p>
            <p></p>
          </div>
        </div>
      </div>
    </div>
END;
    return $html;
    }

    private function formConnexion()
    {
        $html = <<<END
<form class="form-horizontal" method="post" action="#">
                <div class="form-group">
                  <div class="col-sm-2">
                    <label class="control-label">Email</label>
                  </div>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="mail" name="mail" placeholder="E-Mail">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-2">
                    <label  class="control-label">Mot de passe</label>
                  </div>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="mdp" name="mdp" placeholder="Mot de passe">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox">Se souvenir de moi</label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-info">Valider</button>
                    <p>
                      <br>Ou <a href="#">M'enregistrer</a>
                      <br>
                      <br>
                    </p>
                  </div>
                </div>
              </form>
END;
        return $html;

    }

    private function errorConnexion()
    {
        $app = \Slim\Slim::getInstance();
        $relogin = $app->urlFor("connexionForm");

        $html = "<div class=\"connexion-Error\">
                    <p> Erreur : mauvais login / mdp <p><br><br>
                    <p> <a href=$relogin> Retour a la connexion </a> </p>
                </div>";
        return $html;

    }

    private function abonnements()
    {

        $html = <<<END

        <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h2 class="text-center">Vos abonnements</h2>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <table class="table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nombre d'entrées</th>
                  <th>Type</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
END;


         foreach($this->tab as $abo)
        {
            $html.= <<<END
            <tr>
                <td> $abo->id</td>
                <td> $abo->nb_entrees</td>
                <td> $abo->type</td>;
            </tr>
END;

        }
        $html .= '</tbody>
                </table>
            </div>
        </div>
   </div>
</div>';

        return $html;
    }


    private function billets()
    {
        $html = <<<END

        <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h2 class="text-center">Vos billets</h2>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <table class="table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Type</th>
                </tr>
              </thead>
              <tbody>
END;


        foreach($this->tab as $bil)
        {
            $html.= <<<END
            <tr>
                <td> $bil->id</td>
                <td> $bil->type</td>;
            </tr>
END;

        }
        $html .= '</tbody>
                </table>
            </div>
        </div>
   </div>
</div>';

        return $html;
    }

    private function listEntrees()
    {
        $app = \Slim\Slim::getInstance();

        $html = <<<END
                <div>
                    <h2>Acheter des entrées</h2>
                </div>'
                <div class = "abonnements">
                    <h3> Achat d abonnements</h3>
                </div>
            <div class="container">
                    <form action="#" method="post" role="form">
              <div class="form-group">
                <label class="control-label" for="exampleInputPassword1">Nombre de billet en groupe</label>
                <input type="number" class="form-control" name="nbAbo"
                id="exampleInputPassword1" placeholder="Entrez la valeur" min="1" value="1">
              </div>
              <button type="submit" class="btn btn-default">Ajouter</button>
             </form>
            <div>
END;
        return $html;

    }

    private function listMateriel()
    {
        $listm = Materiel::all();
        foreach($listm as $m)
        {

        }
    }

    private function formFamille()
    {

    }

    private function panier()
    {
        $app = \Slim\Slim::getInstance();
        $articles = $_SESSION['panier'];


    }

    private function errorIdentification()
    {
        $app = \Slim\Slim::getInstance();
        $retour = $app->urlFor("accueil");

        $html = "<div class=\"identification-Error\">
                    <p> Erreur : vous devez être connecté(e) pour pouvoir accéder à ce service <p><br><br>
                    <p> <a href=$retour> Retour à l'accueil </a> </p>
                </div>";
        return $html;
    }


}