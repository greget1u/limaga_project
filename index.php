<?php

require_once 'vendor/autoload.php';

use \limaga\control\AdminController as AdminController;
use \limaga\control\ClientController as ClientController;
use \arf\ConnectionFactory as ConnectionFactory;

ConnectionFactory::makeConnection("src/conf/db.etuapp.conf.ini");

$app = new \Slim\Slim();

session_start();


// ACTIONS AVEC CLIENTCONTROLLER
/**
 * Accueil
 */
$app->get('/', function(){
    $c = new ClientController();
    $c->home();
})->name('accueil');


$app->get('/abonnements', function() {
    $c = new ClientController();
    if (isset($_SESSION['client']))
    {
        $c->listAbonnement($_SESSION['client']->id);
    }
    else
    {
        $c->identificationError();
    }

})->name('abonnements');

$app->get('/billets', function(){
    $c = new ClientController();
    if (isset($_SESSION['client']))
    {
        $c->listBillet($_SESSION['client']->id);
    }
    else
    {
        $c->identificationError();
    }
})->name('billets');


$app->get('/panier', function() {
    $c = new ClientController();
    if (isset($_SESSION['client']))
    {
        $c->panier($_SESSION['client']->id);
    }
    else
    {
        $c->identificationError();
    }
})->name('panier');

$app->get('/entrees', function() {
   $c = new ClientController();
    $c->achatEntrees();
})->name('entrees');

$app->get('/produits', function(){
    $c = new ClientController();
    $c->listProduits();
});




// ACTIONS AVEC ADMINCONTROLLER

$app->get('/inscription', function(){
    $c = new AdminController();
    $c->inscriptionForm();
})->name('inscriptionForm');

$app->post('/inscription', function(){
    $c = new AdminController();
    $c->register();
})->name('inscription');

$app->get('/connexion', function(){
    $c = new AdminController();
    $c->loginForm();
})->name('connexionForm');

$app->get('/connexion/error', function(){
   $c = new AdminController();
    $c->loginError();
})->name('connexion/err');

$app->post('/connexion', function(){
    $c = new AdminController();
    $c->logIn();
})->name('connexion');

$app->get('/logout', function()
{
    $c = new AdminController();
    $c->logOut();
});

$app->get('/ajoutFamille', function(){

});

$app->post('/ajoutFamille', function(){

});

$app->run();