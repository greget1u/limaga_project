SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `limaga`
--

-- Table Client

CREATE TABLE IF NOT EXISTS `Client` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `nom` varchar(32) NOT NULL,
  `prenom` varchar(32) NOT NULL,
  `datenaiss` date DEFAULT NULL,
  `adresse` varchar(256) DEFAULT NULL,
  `ville` varchar(64) DEFAULT NULL,
  `code_postal` varchar(6) DEFAULT NULL,
  `mail` varchar(128) DEFAULT NULL,
  `mdp` varchar(256) NOT NULL,
  `telephone` varchar(16) DEFAULT NULL,
  `lvl_natation` int(1),
  PRIMARY KEY (`id`)
) ENGINE =InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `Client` (`id`, `nom`, `prenom`, `datenaiss`, `adresse`, `ville`, `code_postal`, `mail`, `mdp`, `telephone`, `lvl_natation`) VALUES 
(1, 'Dupont', 'Jean', '1990-01-01', '46, rue des prés', 'Nancy', '54000', 'dupont.jean@gmail.com', 'couscous', '0303030303', 2);

-- Table Produit

CREATE TABLE IF NOT EXISTS `Produit` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `nom` varchar(256) NOT NULL,
  `prix` int(5),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Table Famille

CREATE TABLE IF NOT EXISTS `Famille` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `nom` varchar(32) NOT NULL,
  `prenom` varchar(32) NOT NULL,
  `age` int(5) DEFAULT '0',
  `lvl_natation` int(1) DEFAULT '0',
  `id_client` int(8),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- Table Billet 

CREATE TABLE IF NOT EXISTS `Billet` (
	`id` int(8) NOT NULL AUTO_INCREMENT,
	`id_client` int(8) DEFAULT NULL,
	`billet_type` varchar(30) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;

-- Table Abonnement

CREATE TABLE IF NOT EXISTS `Abonnement` (
	`id` int(8) NOT NULL AUTO_INCREMENT,
	`id_client` int(11) DEFAULT NULL,
	`nb_entrees` int(5) DEFAULT '0',
	PRIMARY KEY (`id`)

) ENGINE = InnoDB DEFAULT CHARSET=utf8;

-- Table lecon

CREATE TABLE IF NOT EXISTS `Lecon`(
	`id` int (8) NOT NULL AUTO_INCREMENT,
	`id_client` int(5) DEFAULT NULL,
	PRIMARY KEY(`id`)
)ENGINE = InnoDB DEFAULT CHARSET=utf8;

-- Table Materiel

CREATE TABLE IF NOT EXISTS `Materiel`(
	`id` int(8) NOT NULL AUTO_INCREMENT,
	`libelle` varchar(50) NOT NULL,
	`prix` double(5,2) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8;